cmake_minimum_required(VERSION 3.8 FATAL_ERROR)

project(QsSqlite)

if (NOT CMAKE_BUILD_TYPE)
    message(STATUS "Setting build type to 'Release' as none was specified.")
    set(CMAKE_BUILD_TYPE "Release" CACHE STRING
        "Choose the type of build." FORCE)
endif()

find_package(Threads REQUIRED)
find_package(Qt5 COMPONENTS Core REQUIRED)

option(BUILD_SHARED_LIBS "Build as shared libraries" ON)
option(QSSQLITE_ENABLE_LOW_API "Enable library low level API" OFF)
option(QSSQLITE_ENABLE_UNICODE_API "Enable Unicode collation API" OFF)

# Options to link to SQLite3 system library on Unix-like systems
if (UNIX AND NOT CYGWIN)
    include(CMakeDependentOption)
    option(QSSQLITE_USE_SYSTEM_SQLITE3
        "Try find and link to SQLite3 system library." OFF)
    CMAKE_DEPENDENT_OPTION(QSSQLITE_FORCE_USE_SYSTEM_SQLITE3
        "Force search and linkage to SQLite3 system library." OFF
        "USE_SYSTEM_SQLITE3" OFF)
endif (UNIX AND NOT CYGWIN)

add_library(QsSqlite "")

target_compile_features(QsSqlite PRIVATE cxx_std_14)

if (QSSQLITE_ENABLE_LOW_API)
    target_compile_definitions(QsSqlite PRIVATE -DQS_LOW_API)
endif()

set_target_properties(QsSqlite PROPERTIES
    CXX_EXTENSIONS OFF
    THREADS_PREFER_PTHREAD_FLAG ON
    AUTOMOC ON)

target_include_directories(QsSqlite
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/include/)

target_sources(QsSqlite
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/include/qsstatement.h
        ${CMAKE_CURRENT_LIST_DIR}/include/qsconnection.h
        ${CMAKE_CURRENT_LIST_DIR}/include/qsconnectionconfig.h
        ${CMAKE_CURRENT_LIST_DIR}/include/qsconnectionworker.h
        ${CMAKE_CURRENT_LIST_DIR}/include/qsconnectionasyncworker.h
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/src/qsstatement.cpp
        ${CMAKE_CURRENT_LIST_DIR}/src/qsconnection.cpp
        ${CMAKE_CURRENT_LIST_DIR}/src/qshelper.cpp
        ${CMAKE_CURRENT_LIST_DIR}/src/qshelper.h
        ${CMAKE_CURRENT_LIST_DIR}/src/qsconnectionconfig.cpp
        ${CMAKE_CURRENT_LIST_DIR}/src/qsconnectionworker.cpp
        ${CMAKE_CURRENT_LIST_DIR}/src/qsconnectionasyncworker.cpp)

if (QSSQLITE_ENABLE_UNICODE_API)
    find_package(ICU COMPONENTS i18n REQUIRED)
    if (ICU_FOUND)
        target_include_directories(QsSqlite PRIVATE ${ICU_INCLUDE_DIRS})
        target_compile_definitions(QsSqlite PRIVATE -DQS_UNICODE_COLL)
        target_link_libraries(QsSqlite ICU::i18n)
    else()
        message(FATAL_ERROR "ICU components not found in system.")
    endif()
endif(QSSQLITE_ENABLE_UNICODE_API)

if (QSSQLITE_USE_SYSTEM_SQLITE3)
    set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_CURRENT_SOURCE_DIR}/cmake")
    find_package(SQLite3)
    if (SQLITE3_FOUND)
        target_include_directories(QsSqlite PRIVATE ${SQLITE3_INCLUDE_DIRS})
        target_link_libraries(QsSqlite ${SQLITE3_LIBRARIES})
    elseif (NOT QSSQLITE_FORCE_USE_SYSTEM_SQLITE3)
        set(QSSQLITE_USE_PROJECT_SQLITE3 ON)
    else()
        message(FATAL_ERROR "SQLite3 library not found in system.")
    endif (SQLITE3_FOUND)
else()
    set(QSSQLITE_USE_PROJECT_SQLITE3 ON)
endif (QSSQLITE_USE_SYSTEM_SQLITE3)

if (QSSQLITE_USE_PROJECT_SQLITE3)
    add_subdirectory(sqlite)
    target_link_libraries(QsSqlite sqlite)
endif (QSSQLITE_USE_PROJECT_SQLITE3)

target_link_libraries(QsSqlite Threads::Threads)
target_link_libraries(QsSqlite Qt5::Core)
