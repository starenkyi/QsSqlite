#include "../include/qsconnection.h"

#include <cstring>
#include <atomic>

#include <QCollator>
#include <QReadLocker>
#include <QWriteLocker>

#ifdef QS_UNICODE_COLL
#include "unicode/utypes.h"
#include "unicode/ucol.h"
#include "unicode/uvernum.h"
#endif

#include "sqlite3.h"
#include "qsstatement.h"


static const char* const cUTF8 {"UTF-8"};
static const char* const cUTF16LE {"UTF-16le"};
static const char* const cUTF16BE {"UTF-16be"};
static const char* const cUTF16 {"UTF-16"};
static const QByteArray cEncodingQuery {QByteArrayLiteral("pragma encoding")};


namespace {

int getOpenFlags(const QsConnection::OpenMode   openMode,
                 const QsConnection::ThreadMode threadMode,
                 const QsConnection::CacheMode  cacheMode) noexcept
{
    int resFlags = 0;

    // set uri open mode
    switch (openMode) {
    case QsConnection::OpenMode::ReadWriteCreate:
    case QsConnection::OpenMode::InMemory:
        resFlags |= SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE;
        break;
    case QsConnection::OpenMode::ReadWrite:
        resFlags |= SQLITE_OPEN_READWRITE;
        break;
    case QsConnection::OpenMode::ReadOnly:
        resFlags |= SQLITE_OPEN_READONLY;
        break;
    }

    // set thread mode
    switch (threadMode) {
    case QsConnection::ThreadMode::MultiThread:
        resFlags |= SQLITE_OPEN_NOMUTEX;
        break;
    case QsConnection::ThreadMode::Serialized:
        resFlags |= SQLITE_OPEN_FULLMUTEX;
        break;
    default:
        break;
    }

    // set cache mode
    switch (cacheMode) {
    case QsConnection::CacheMode::PrivateCache:
        resFlags |= SQLITE_OPEN_PRIVATECACHE;
        break;
    case QsConnection::CacheMode::SharedCache:
        resFlags |= SQLITE_OPEN_SHAREDCACHE;
        break;
    }

    return resFlags;
}

// function to compare UTF16 string by locale
// (collator is a pointer to some QCollator object)
int localeCompareUtf16(void* collator,
                       int firstLength,
                       const void* firstStr,
                       int secondLength,
                       const void* secondStr)
{
    Q_ASSERT(collator);
    return reinterpret_cast<QCollator*>(collator)->compare(
                reinterpret_cast<const QChar*>(firstStr), firstLength / 2,
                reinterpret_cast<const QChar*>(secondStr), secondLength / 2);
}

#ifdef QS_UNICODE_COLL

int icuLocalCompareUtf16(void* collator,
                         int firstLength,
                         const void* firstStr,
                         int secondLength,
                         const void* secondStr)
{
    Q_ASSERT(collator);
    return ucol_strcoll(reinterpret_cast<const UCollator*>(collator),
                        reinterpret_cast<const UChar*>(firstStr),
                        firstLength / 2,
                        reinterpret_cast<const UChar*>(secondStr),
                        secondLength / 2);
}

int icuLocalCompareUtf8(void* collator,
                        int firstLength,
                        const void* firstStr,
                        int secondLength,
                        const void* secondStr)
{
    Q_ASSERT(collator);
    return ucol_strcollUTF8(reinterpret_cast<const UCollator*>(collator),
                            reinterpret_cast<const char*>(firstStr),
                            firstLength,
                            reinterpret_cast<const char*>(secondStr),
                            secondLength, NULL);
}

#endif

}


using DoubleResult   = std::pair<double,     int>;
using Int64Result    = std::pair<qint64,     int>;
using StringResult   = std::pair<QByteArray, int>;
using String16Result = std::pair<QString,    int>;


class QsConnection::Collator
{
public:

    typedef int (*CompareSignature)(void*, int, const void*, int, const void*);

    Collator(const QLocale&               locale,
             const QsConnection::Encoding encoding)
        : _encoding {encoding},
          _collator {std::make_unique<QCollator>(locale)}
#ifdef QS_UNICODE_COLL
        , _icuCollator {nullptr}
#endif
    {
    }

#ifdef QS_UNICODE_COLL

    Collator(const QByteArray&            localeOptions,
             const QsConnection::Encoding encoding)
        : _encoding {encoding},
          _collator {nullptr},
          _icuCollator {nullptr}
    {
        UErrorCode errCode;
        _icuCollator = ucol_open(localeOptions.constData(), &errCode);
    }

    ~Collator()
    {
        if (_icuCollator) {
            ucol_close(_icuCollator);
        }
    }

#endif

    CompareSignature compareFunction() const noexcept
    {
        if (_collator) {
            return localeCompareUtf16;
        }
#ifdef QS_UNICODE_COLL
        else if (_icuCollator) {
            return (_encoding == QsConnection::UTF8)
                    ? icuLocalCompareUtf8 : icuLocalCompareUtf16;
        }
#endif
        else {
            return NULL;
        }
    }

    int sqliteEncoding() const noexcept
    {
        switch (_encoding) {
        case QsConnection::UTF8:
            return SQLITE_UTF8;
        case QsConnection::UTF16:
            return SQLITE_UTF16;
        case QsConnection::UTF16LE:
            return SQLITE_UTF16LE;
        case QsConnection::UTF16BE:
            return SQLITE_UTF16BE;
        default:
            return -1;
        }
    }

    bool hasEncoding(const QsConnection::Encoding encoding) const noexcept
    {
        return _encoding == encoding;
    }

    bool isValid() const noexcept
    {
        return _collator.get()
#ifdef QS_UNICODE_COLL
                || _icuCollator
#endif
        ;
    }

    void* pointerToObject() const noexcept
    {
#ifdef QS_UNICODE_COLL
        if (_icuCollator) {
            return _icuCollator;
        } else {
            return _collator.get();
        }
#else
        return _collator.get();
#endif
    }

    Collator() = delete;
    Collator(const Collator&) = delete;
    Collator(Collator&&) = delete;
    Collator& operator=(const Collator&) = delete;
    Collator& operator=(Collator&&) = delete;

private:
    const QsConnection::Encoding _encoding;
    const std::unique_ptr<QCollator> _collator;
#ifdef QS_UNICODE_COLL
    UCollator *_icuCollator;
#endif
};


QsConnection::QsConnection(const QByteArray& dbName) Q_DECL_NOTHROW
    : _db {NULL},
      _encoding {Encoding::Undefined},
      _dbName {dbName}
{}

QsConnection::QsConnection(QsConnection&& connection) Q_DECL_NOTHROW
    : _db {connection._db},
      _encoding {connection._encoding},
      _dbName {std::move(connection._dbName)},
      _openErrorMsg {std::move(connection._openErrorMsg)},
      _collators {std::move(connection._collators)}
{
    connection.reset();
}

QsConnection::~QsConnection()
{
    close();
}

void QsConnection::close() Q_DECL_NOTHROW
{
    // check if connection is opened
    if (Q_LIKELY(isOpen())) {
        // close connection and reset
        sqlite3_close_v2(_db);
        _db = NULL;

        // reset cached encoding; clear open error message and collators list
        _encoding = Undefined;
        _openErrorMsg.clear();
        deleteAllCollators();
    }
}

bool QsConnection::commit() Q_DECL_NOTHROW
{
    return execute(QByteArrayLiteral("commit"));
}

bool QsConnection::createUtf16Collation(const QByteArray& name,
                                        const QLocale&    locale)
{
    bool success {false};

    // check if collation not exists and DB has final encoding
    if (Q_LIKELY(isOpen() && !_collators.contains(name) && encodingWasSet()
                 && (_encoding == UTF16LE || _encoding == UTF16BE))) {

        // create collator; try register and save it
        success = registerCollation(name, std::make_unique<Collator>(
                                        locale, _encoding));
    }

    return success;
}

#ifdef QS_UNICODE_COLL

bool QsConnection::createIcuCollation(const QByteArray& name,
                                      const QByteArray& localeOptions)
{
    bool success {false};

    // check if options is not empty and collation not exists
    if (Q_LIKELY(!localeOptions.isEmpty() && isOpen()
                 && !_collators.contains(name))) {

        // find and check encoding of the database
        if (encodingWasSet()) {

            // create collator; try register and save it
            success = registerCollation(name, std::make_unique<Collator>(
                                            localeOptions, _encoding));
        }
    }
    return success;
}

#endif

bool QsConnection::deleteCollation(const QByteArray& name)
{
    bool rSuccess {false};

    // check if connection is opened and try delete collation for it
    if (Q_LIKELY(isOpen())) {

        // found and delete collator object; delete collation from connection
        const auto it {qAsConst(_collators).find(name)};
        if (it != _collators.cend() && Q_LIKELY(it.value())) {
            rSuccess = sqlite3_create_collation_v2(
                        _db, name.constData(), it.value()->sqliteEncoding(),
                        NULL, NULL, NULL) == SQLITE_OK;
            delete it.value();
            _collators.erase(it);
        }
    }

    // return false if connection is closed or collation deleting failed
    return rSuccess;
}

QsConnection::Encoding QsConnection::encoding() const
{
    Encoding rEncoding {_encoding};

    // if database is open and encoding was not cached
    // then read encoding value and cache the value, if the database
    // was already created (in this case encoding cannot be changed in feature)
    if (Q_UNLIKELY(isOpen() && _encoding == Undefined)) {
        rEncoding = readEncoding();
        if (databaseWasCreated()) {
            _encoding = rEncoding;
        }
    }
    return rEncoding;
}

bool QsConnection::execute(const QByteArray& query) const noexcept
{
    // execute query if connection is opened
    return isOpen() && sqlite3_exec(_db, query.constData(),
                                    NULL, NULL, NULL) == SQLITE_OK;
}

bool QsConnection::execute(const QString& query) const
{
    return execute(query.toUtf8());
}

QByteArray QsConnection::databaseName() const Q_DECL_NOTHROW
{
    return _dbName;
}

bool QsConnection::isOpen() const noexcept
{
    return _db;
}

int QsConnection::lastErrorCode() const noexcept
{
    return Q_LIKELY(isOpen()) ? sqlite3_errcode(_db)
                              : ReadResult::ConnectionIsClosed;
}

QByteArray QsConnection::lastError() const
{
    QByteArray result;

    // try get error
    if (Q_LIKELY(isOpen())) {
        result = sqlite3_errmsg(_db);
    } else {
        result = _openErrorMsg;
    }

    return result;
}

QString QsConnection::lastError16() const
{
    QString result;

    // try get error (if database is opened)
    if (Q_LIKELY(isOpen())) {
        result = QString(reinterpret_cast<const QChar*>(sqlite3_errmsg16(_db)));
    } else {
        result = _openErrorMsg;
    }

    return result;
}

qint64 QsConnection::lastInsertRowId() const noexcept
{
    return Q_LIKELY(isOpen()) ? sqlite3_last_insert_rowid(_db) : 0;
}

bool QsConnection::open(OpenMode   openMode,
                        ThreadMode threadMode,
                        CacheMode  cacheMode)
{
    // check connection is not opened
    if (Q_LIKELY(!isOpen())) {

        // try open database
        const int code {(openMode != OpenMode::InMemory)
                ? openRegularDb(getOpenFlags(openMode, threadMode, cacheMode))
                : openInMemoryDb(cacheMode)};

        // check result
        if (Q_UNLIKELY(code != SQLITE_OK)) {
            // read and save last error
            _openErrorMsg = sqlite3_errmsg(_db);

            // release sqlite3 pointer and return false
            sqlite3_close_v2(_db);
            _db = NULL;
            return false;
        } else {
            _openErrorMsg.clear();
        }
    }

    // return true (connection is opened, or connection was opened before)
    return true;
}

QsStatement QsConnection::prepare(const QByteArray& query) Q_DECL_NOTHROW
{
    return QsStatement(*this, query);
}

QsStatement QsConnection::prepare(const QString& query) Q_DECL_NOTHROW
{
    return QsStatement(*this, query);
}

DoubleResult QsConnection::readDouble(const QByteArray& query) const
{
    DoubleResult result;

    // try read double and save read result code
    result.second = readValue(query,
                              [&result] (sqlite3_stmt* stmt) -> void {
        result.first = sqlite3_column_double(stmt, 0);
    });

    // return result
    return result;
}

DoubleResult QsConnection::readDouble(const QString& query) const
{
    return readDouble(query.toUtf8());
}

Int64Result QsConnection::readInt64(const QByteArray& query) const
{
    Int64Result result;

    // try read int64 and save read result code
    result.second = readValue(query,
                              [&result] (sqlite3_stmt* stmt) -> void {
        result.first = sqlite3_column_int64(stmt, 0);
    });

    // return result
    return result;
}

Int64Result QsConnection::readInt64(const QString& query) const
{
    return readInt64(query.toUtf8());
}

StringResult QsConnection::readString(const QByteArray& query) const
{
    StringResult result;

    // try read string and save read result code
    result.second = readValue(query, [&result] (sqlite3_stmt* stmt) -> void {
        const auto data {reinterpret_cast<const char*>(
                sqlite3_column_text(stmt, 0))};
        const auto length {sqlite3_column_bytes(stmt, 0)};
        result.first = QByteArray(data, length);
    });

    // return result
    return result;
}

StringResult QsConnection::readString(const QString& query) const
{
    return readString(query.toUtf8());
}

String16Result QsConnection::readString16(const QByteArray& query) const
{
    String16Result result;

    // try read string and save read result code
    result.second = readValue(query, [&result] (sqlite3_stmt* stmt) -> void {
        const auto data {reinterpret_cast<const QChar*>(
                sqlite3_column_text16(stmt, 0))};
        const auto length {sqlite3_column_bytes16(stmt, 0)};
        result.first = QString(data, length / 2);
    });

    // return result
    return result;
}

String16Result QsConnection::readString16(const QString& query) const
{
    return readString16(query.toUtf8());
}

bool QsConnection::rollback() Q_DECL_NOTHROW
{
    return execute(QByteArrayLiteral("rollback"));
}

void QsConnection::setDatabaseName(const QByteArray& dbName) Q_DECL_NOTHROW
{
    // check if connection is not openned and assign value
    if (!isOpen()) {
        _dbName = dbName;
    }
}

bool QsConnection::setEncoding(const Encoding newEncoding)
{
    bool success {Q_LIKELY(isOpen()) && newEncoding != Undefined};
    if (Q_LIKELY(success)) {
        const Encoding currentEncoding {encoding()};

        // don't change encoding if it is the same as current encoding
        if (!(success = (newEncoding == currentEncoding))) {
            // don't try to change encoding if the encoding was cached
            // (SQLite forbid changing encoding for created DB)
            if ((success = (_encoding == Undefined))) {
                const char* encodingPtr {nullptr};
                switch (newEncoding) {
                case UTF8:
                    encodingPtr = cUTF8;
                    break;
                case UTF16:
                    encodingPtr = cUTF16;
                    break;
                case UTF16LE:
                    encodingPtr = cUTF16LE;
                    break;
                case UTF16BE:
                    encodingPtr = cUTF16BE;
                    break;
                default:
                    break;
                }

                // try change encoding
                if (Q_LIKELY(encodingPtr)
                             && execute(QByteArray(cEncodingQuery).append("=\'")
                                        .append(encodingPtr).append('\''))) {
                    // check new value of database encoding
                    const Encoding dbEncoding {encoding()};
                    success = (newEncoding == dbEncoding)
                            || (newEncoding == UTF16 && (dbEncoding == UTF16LE
                                                    || dbEncoding == UTF16BE));


                    // delete all registered collation if one of them (it means
                    // all of them) has different encoding from new
                    // (updated) encoding
                    if (!_collators.isEmpty() && !(_collators.cbegin().value()->
                            hasEncoding(dbEncoding))) {
                        deleteAllCollators();
                    }
                }
            }
        }
    }

    return success;
}

bool QsConnection::transaction() Q_DECL_NOTHROW
{
    return execute(QByteArrayLiteral("begin"));
}

QsConnection& QsConnection::operator =(QsConnection&& connection) Q_DECL_NOTHROW
{
    if (this != &connection) {
        // close current connection
        close();

        // move assign object vars
        _db = connection._db;
        _encoding = connection._encoding;
        _dbName = std::move(connection._dbName);
        _openErrorMsg = std::move(connection._openErrorMsg);
        _collators = std::move(connection._collators);

        // reset moved object
        connection.reset();
    }

    return *this;
}

bool QsConnection::encodingWasSet() const
{
    // check if DB encoding was cached because the QsConnection object cache
    // the encoding value only if it cannot be changed in the feature.
    // INFO: encoding of SQLite DB cannot be changed after creating DB schema
    return (_encoding != Undefined)
            || (_encoding == readEncoding());
}

void QsConnection::deleteAllCollators()
{
    for (auto it {_collators.cbegin()}; it != _collators.cend(); ++it) {
        delete it.value();
    }
    _collators.clear();
}

bool QsConnection::databaseWasCreated() const
{
    bool rWasCreated {false};

    // read number of rows from 'sqlite_master' table:
    // if database was already created then the table is not empty;
    // otherwise there is not guarantee that the database was created
    const auto dataCount {readInt64(
                    QByteArrayLiteral("select count(*) from sqlite_master"))};
    if (dataCount.second == ReadSuccess && dataCount.first > 0) {
        rWasCreated = true;
    }
    return rWasCreated;
}

int QsConnection::openInMemoryDb(CacheMode cacheMode)
{
    // build URI string
    QByteArray uriStr("file:");
    if (_dbName.isEmpty()) {
        uriStr += ":memory:?cache=";
    } else {
        uriStr += _dbName;
        uriStr += "?mode=memory&cache=";
    }

    uriStr += (cacheMode == CacheMode::PrivateCache) ? "private" : "shared";

    // try open in-memory db and return result code
    return sqlite3_open_v2(uriStr.constData(), &_db, SQLITE_OPEN_URI, NULL);
}

int QsConnection::openRegularDb(const int flags) noexcept
{
    return sqlite3_open_v2(_dbName.constData(), &_db, flags, NULL);
}

QsConnection::Encoding QsConnection::readEncoding() const
{
    Encoding rEncoding {Encoding::Undefined};

    // try read PRAGMA-value of the database encoding
    readValue(cEncodingQuery, [&rEncoding] (sqlite3_stmt* stmt) -> void {
            const char* const encoding {reinterpret_cast<const char*>
                        (sqlite3_column_text(stmt, 0))};
            if (strcmp(encoding, cUTF8) == 0) {
                rEncoding = Encoding::UTF8;
            } else if (strcmp(encoding, cUTF16LE) == 0) {
                rEncoding = Encoding::UTF16LE;
            } else if (strcmp(encoding, cUTF16BE) == 0) {
                rEncoding = Encoding::UTF16BE;
            }
          });
    return rEncoding;
}

int QsConnection::readValue(
        const QByteArray&                          query,
        const std::function<void (sqlite3_stmt*)>& readLambda) const
{
    // check connection
    if (Q_LIKELY(isOpen())) {
        // try prepare statement
        sqlite3_stmt *stmt;
        int resultCode {sqlite3_prepare_v2(_db, query.constData(),
                                           query.length(), &stmt, NULL)};

        // if success, try read data (or set the error code)
        if (Q_LIKELY(resultCode == SQLITE_OK)) {

            // check if statement return any data
            if (Q_LIKELY(sqlite3_column_count(stmt))) {

                // check if statement has prepared row
                if (sqlite3_step(stmt) == SQLITE_ROW) {

                    // check value type is not NULL
                    if (sqlite3_column_type(stmt, 0) != SQLITE_NULL) {

                        // catch potential exception of std::function object
                        try {
                            // read value
                            readLambda(stmt);
                            resultCode = ReadSuccess;
                        } catch (...) {
                            // delete prepared statement and re-throw
                            sqlite3_finalize(stmt);
                            throw;
                        }

                    } else {
                        resultCode = NullValue;
                    }
                } else {
                    resultCode = EmptyData;
                }
            } else {
                resultCode = NoData;
            }

            // delete prepared statement and return result code
            sqlite3_finalize(stmt);
        }

        // return code of sqlite error
        return resultCode;
    } else {
        // return ConnectionIsClosed error code
        return ConnectionIsClosed;
    }
}

bool QsConnection::registerCollation(const QByteArray&         name,
                                     std::unique_ptr<Collator> collator)
{
    Q_ASSERT(collator);

    bool success {false};

    // try register collator and return true on success
    if (Q_LIKELY(collator->isValid())
            && sqlite3_create_collation_v2(
                _db, name.constData(), collator->sqliteEncoding(),
                collator->pointerToObject(), collator->compareFunction(), NULL)
            == SQLITE_OK) {

        // catch potential exception because of saving collator
        try {
            // save collator to collators hashtable
            _collators.insert(name, collator.get());

            // release pointer to collator from temporary smart pointer
            collator.release();

            success = true;
        } catch (...) {
            // delete registered collator from connection and re-throw
            sqlite3_create_collation_v2(_db, name.constData(),
                                        collator->sqliteEncoding(),
                                        NULL, NULL, NULL);
            throw;
        }
    }
    return success;
}

void QsConnection::reset() Q_DECL_NOTHROW
{
    // reset all fields to default values
    _db = NULL;
    _encoding = Undefined;
    _dbName.clear();
    _openErrorMsg.clear();

    // reset is used after moving the object data, so all collators (saved
    // using raw pointers) are already moved out and there is not a memory leak
    _collators.clear();
}

