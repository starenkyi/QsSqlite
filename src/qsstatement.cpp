#include "../include/qsstatement.h"

#include <cstring>

#include <QMetaType>
#include <QtGlobal>

#include "sqlite3.h"
#include "qsconnection.h"

#define DESTRUCTOR_FOR(cacheValue) cacheValue ? SQLITE_TRANSIENT : SQLITE_STATIC

#define CHECK_STATEMENT(stmt, methodName) \
    Q_ASSERT_X(stmt->isValid(), methodName, "Statement is invalid");

#define CHECK_READ_STMT_TYPE(stmt, methodName) \
    Q_ASSERT_X(stmt->hasSelectType(), methodName, \
               "Statement is not prepared to read values");

#define CHECK_BIND_INDEX(stmt, index, methodName) \
    Q_ASSERT_X(index > 0 && index <= stmt->bindCount(), methodName, \
               "Index out of range");

#define CHECK_READ_INDEX(stmt, index, methodName) \
    Q_ASSERT_X(index >= 0 && index < stmt->columnCount(), methodName, \
               "Index out of range");

#define CHECK_BIND_ARGS(stmt, methodName, index) \
    CHECK_STATEMENT(stmt, methodName) \
    CHECK_BIND_INDEX(stmt, index, methodName)

#define CHECK_READ_ARGS(stmt, methodName, index) \
    CHECK_STATEMENT(stmt, methodName) \
    CHECK_READ_STMT_TYPE(stmt, methodName) \
    CHECK_READ_INDEX(stmt, index, methodName)

QsStatement::QsStatement() noexcept
    : _statement {NULL},
      _db {NULL},
      _encoding {QsConnection::Undefined}
{}

QsStatement::QsStatement(const QsConnection& connection,
                         const QByteArray&   query) noexcept
    : _statement {NULL},
      _db {connection._db},
      _encoding {connection.encoding()}
{
    // try compile statement and check result
    if (!compile(query)) {
        reset();
    }
}

QsStatement::QsStatement(const QsConnection& connection,
                         const QString&      query) noexcept
    : _statement {NULL},
      _db {connection._db},
      _encoding {connection.encoding()}
{
    // try compile statement and check result
    if (!compile(query)) {
        reset();
    }
}

QsStatement::QsStatement(QsStatement&& statement) noexcept
    : _statement {statement._statement},
      _db {statement._db},
      _encoding {statement._encoding}
{
    statement.reset();
}

QsStatement::~QsStatement()
{
    clear();
}

#if defined(QS_LOW_API)

bool QsStatement::bindBlob(const int         index,
                           const void* const value,
                           const int         bytes,
                           const bool        cacheValue) const noexcept
{
    CHECK_BIND_ARGS(this, "bindBlob", index)
    Q_ASSERT_X(bytes >= 0, "bindBlob", "bytes mustn't be negative number");

    return sqlite3_bind_blob(_statement, index, value, bytes,
                             DESTRUCTOR_FOR(cacheValue)) == SQLITE_OK;
}

#endif

bool QsStatement::bindBlob(const int         index,
                           const QByteArray& value,
                           const bool        cacheValue) const noexcept
{
    CHECK_BIND_ARGS(this, "bindBlob", index)

    return sqlite3_bind_blob(_statement, index, value.constData(),
                             value.length(),
                             DESTRUCTOR_FOR(cacheValue)) == SQLITE_OK;
}

bool QsStatement::bindBool(const int  index,
                           const bool value) const noexcept
{
    CHECK_BIND_ARGS(this, "bindBool", index)

    return sqlite3_bind_int(_statement, index, value) == SQLITE_OK;
}

int QsStatement::bindCount() const noexcept
{
    return isValid() ? sqlite3_bind_parameter_count(_statement) : -1;
}

bool QsStatement::bindDouble(const int    index,
                             const double value) const noexcept
{
    CHECK_BIND_ARGS(this, "bindDouble", index)

    return sqlite3_bind_double(_statement, index, value) == SQLITE_OK;
}

bool QsStatement::bindInt(const int index,
                          const int value) const noexcept
{
    CHECK_BIND_ARGS(this, "bindInt", index)

    return sqlite3_bind_int(_statement, index, value) == SQLITE_OK;
}

bool QsStatement::bindInt64(const int    index,
                            const qint64 value) const noexcept
{
    CHECK_BIND_ARGS(this, "bindInt64", index)

    return sqlite3_bind_int64(_statement, index, value) == SQLITE_OK;
}

bool QsStatement::bindNull(const int index) const noexcept
{
    CHECK_BIND_ARGS(this, "bindNull", index)

    return sqlite3_bind_null(_statement, index) == SQLITE_OK;
}

#if defined(QS_LOW_API)

bool QsStatement::bindText(const int         index,
                           const char* const value,
                           const int         bytes,
                           const bool        cacheValue) const noexcept
{
    CHECK_BIND_ARGS(this, "bindText", index)

    return sqlite3_bind_text(_statement, index, value,
                             bytes, DESTRUCTOR_FOR(cacheValue)) == SQLITE_OK;
}

#endif

bool QsStatement::bindText(const int         index,
                           const QByteArray& value,
                           const bool        cacheValue) const noexcept
{
    CHECK_BIND_ARGS(this, "bindText", index)

    return sqlite3_bind_text(_statement, index, value.constData(),
                             value.length(),
                             DESTRUCTOR_FOR(cacheValue)) == SQLITE_OK;
}

#if defined(QS_LOW_API)

bool QsStatement::bindText16(const int         index,
                             const void* const value,
                             const int         bytes,
                             const bool        cacheValue) const noexcept
{
    CHECK_BIND_ARGS(this, "bindText16", index)

    return sqlite3_bind_text16(_statement, index, value,
                               bytes, DESTRUCTOR_FOR(cacheValue)) == SQLITE_OK;
}

#endif

bool QsStatement::bindText16(const int      index,
                             const QString& value,
                             const bool     cacheValue) const noexcept
{
    CHECK_BIND_ARGS(this, "bindText16", index)

    return sqlite3_bind_text16(_statement, index, value.constData(),
                               value.length() * 2,
                               DESTRUCTOR_FOR(cacheValue)) == SQLITE_OK;
}

bool QsStatement::bindValue(int             index,
                            const QVariant& value,
                            bool            cacheValue) const noexcept
{
    CHECK_BIND_ARGS(this, "bindValue", index)

    if (value.isNull()) {
        return bindNull(index);
    } else {
        switch (static_cast<QMetaType::Type>(value.type())) {
            case QMetaType::Bool:
                return bindBool(index, value.toBool());
            case QMetaType::Int:
            case QMetaType::UInt:
            case QMetaType::LongLong:
            case QMetaType::ULongLong:
            case QMetaType::QChar:
            case QMetaType::Long:
            case QMetaType::ULong:
                return bindInt64(index, value.toLongLong());
            case QMetaType::Double:
            case QMetaType::Float:
                return bindDouble(index, value.toDouble());
            case QMetaType::QByteArray:
                return (dbIsUtf8Encoded())
                        ? bindText(index, value.toByteArray(), cacheValue)
                        : bindText16(index, value.toString(), SQLITE_TRANSIENT);
            case QMetaType::QString:
                return (dbIsUtf8Encoded())
                        ? bindText(index, value.toByteArray(), SQLITE_TRANSIENT)
                        : bindText16(index, value.toString(), cacheValue);
            default:
            {
                if (dbIsUtf8Encoded()) {
                    if (value.canConvert(QMetaType::QByteArray)) {
                        return bindText(index, value.toByteArray(),
                                        SQLITE_TRANSIENT);
                    }
                } else if (value.canConvert(QMetaType::QString)) {
                    return bindText16(index, value.toString(),
                                      SQLITE_TRANSIENT);
                }
            }
        }
    }
    return false;
}

void QsStatement::clear() noexcept
{
    if (isValid()) {
        sqlite3_finalize(_statement);
        reset();
    }
}

void QsStatement::clearBindings() const noexcept
{
    CHECK_STATEMENT(this, "clearBindings")

    sqlite3_clear_bindings(_statement);
}

int QsStatement::columnCount() const noexcept
{
    return hasSelectType() ? sqlite3_column_count(_statement) : -1;
}

QsStatement::DataType QsStatement::columnType(const int index) const noexcept
{
    CHECK_READ_ARGS(this, "columnType", index)

    switch (sqlite3_column_type(_statement, index)) {
    case SQLITE_INTEGER:
        return DataType::Integer;
    case SQLITE3_TEXT:
        return DataType::Text;
    case SQLITE_FLOAT:
        return DataType::Double;
    case SQLITE_BLOB:
        return DataType::Blob;
    case SQLITE_NULL:
    default:
        return DataType::Null;
    }
}

int QsStatement::encoding() const noexcept
{
    return _encoding;
}

bool QsStatement::execute() const noexcept
{
    CHECK_STATEMENT(this, "execute")

    return sqlite3_step(_statement) == SQLITE_DONE;
}

QByteArray QsStatement::expandedQuery() const
{
    QByteArray result;

    if (isValid())
    {
        char* str = sqlite3_expanded_sql(_statement);
        if (str) {
            result = str;
            sqlite3_free(str);
        }
    }

    return result;
}

QString QsStatement::expandedQuery16() const
{
    QString result;

    if (isValid())
    {
        char* str = sqlite3_expanded_sql(_statement);
        if (str) {
            result = QString::fromUtf8(str);
            sqlite3_free(str);
        }
    }

    return result;
}

#if defined(QS_LOW_API)

QsStatement::ConstBinary QsStatement::getBinary(const int index) const noexcept
{
    CHECK_READ_ARGS(this, "getBinary", index)

    const auto data {reinterpret_cast<const unsigned char*>
                (sqlite3_column_blob(_statement, index))};
    const int length {sqlite3_column_bytes(_statement, index)};
    return ConstBinary {data, length};
}

QsStatement::Binary QsStatement::getBinaryCopy(const int index) const
{
    CHECK_READ_ARGS(this, "getBinaryCopy", index)

    const void* const temp = sqlite3_column_blob(_statement, index);
    const int bytes = sqlite3_column_bytes(_statement, index);
    unsigned char* result {nullptr};
    if (bytes) {
        result = new unsigned char[static_cast<size_t>(bytes)];
        memcpy(result, temp, static_cast<size_t>(bytes));
    }

    return Binary {result, bytes};
}

#endif

QByteArray QsStatement::getBlob(const int index) const
{
    CHECK_READ_ARGS(this, "getBlob", index)

    const auto data {reinterpret_cast<const char*>
                (sqlite3_column_blob(_statement, index))};
    const int length {sqlite3_column_bytes(_statement, index)};
    return QByteArray(data, length);
}

bool QsStatement::getBool(const int index) const noexcept
{
    CHECK_READ_ARGS(this, "getBool", index)

    return sqlite3_column_int(_statement, index);
}

QByteArray QsStatement::getConvertedText(const int index) const
{
    CHECK_READ_ARGS(this, "getConvertedText", index)

    const auto data {reinterpret_cast<const QChar*>
                (sqlite3_column_text16(_statement, index))};
    const int length {sqlite3_column_bytes(_statement, index) / 2};
    return QString::fromRawData(data, length).toUtf8();
}

QString QsStatement::getConvertedText16(const int index) const
{
    CHECK_READ_ARGS(this, "getConvertedText16", index)

    const auto data {reinterpret_cast<const char*>
                (sqlite3_column_text(_statement, index))};
    const int length {sqlite3_column_bytes(_statement, index)};
    return QString::fromUtf8(data, length);
}

#if defined(QS_LOW_API)

QsStatement::ConstUtf8 QsStatement::getCText(const int index) const noexcept
{
    CHECK_READ_ARGS(this, "getCText", index)

    const auto data {reinterpret_cast<const char*>
                (sqlite3_column_text(_statement, index))};
    const int length {sqlite3_column_bytes(_statement, index)};
    return ConstUtf8 {data, length};
}

QsStatement::ConstUtf16 QsStatement::getCText16(const int index) const noexcept
{
    CHECK_READ_ARGS(this, "getCText16", index)

    const auto data {reinterpret_cast<const QChar*>
                (sqlite3_column_text16(_statement, index))};
    const int length {sqlite3_column_bytes16(_statement, index) / 2};
    return ConstUtf16 {data, length};
}

QsStatement::Utf8 QsStatement::getCTextCopy(const int index) const
{
    CHECK_READ_ARGS(this, "getCTextCopy", index)

    const void* const from = sqlite3_column_text(_statement, index);
    const int bytes = sqlite3_column_bytes(_statement, index);
    char* result {nullptr};

    if (from) {
        result = new char[static_cast<size_t>(bytes + 1)];
        memcpy(result, from, static_cast<size_t>(bytes) + sizeof(char));
    }

    return Utf8 {result, bytes};
}

QsStatement::Utf16 QsStatement::getCText16Copy(const int index) const
{
    CHECK_READ_ARGS(this, "getCText16Copy", index)

    const void* const from = sqlite3_column_text16(_statement, index);
    const int bytes = sqlite3_column_bytes16(_statement, index);
    const int characters = bytes / 2;

    QChar* result {nullptr};
    if (from) {
        result = new QChar[static_cast<unsigned long>(characters + 1)];
        memcpy(result, from, static_cast<size_t>(bytes) + sizeof(QChar));
    }

    return Utf16 {result, characters};
}

#endif

double QsStatement::getDouble(const int index) const noexcept
{
    CHECK_READ_ARGS(this, "getDouble", index)

    return sqlite3_column_double(_statement, index);
}

int QsStatement::getInt(const int index) const noexcept
{
    CHECK_READ_ARGS(this, "getInt", index)

    return sqlite3_column_int(_statement, index);
}

qint64 QsStatement::getInt64(const int index) const noexcept
{
    CHECK_READ_ARGS(this, "getInt64", index)

    return sqlite3_column_int64(_statement, index);
}

QByteArray QsStatement::getText(const int index) const
{
    CHECK_READ_ARGS(this, "getText", index)

    const auto data {reinterpret_cast<const char*>
                (sqlite3_column_text(_statement, index))};
    const int length {sqlite3_column_bytes(_statement, index)};
    return QByteArray(data, length);
}

QString QsStatement::getText16(const int index) const
{
    CHECK_READ_ARGS(this, "getText16", index)

    const auto data {reinterpret_cast<const QChar*>
                (sqlite3_column_text16(_statement, index))};
    const int length {sqlite3_column_bytes16(_statement, index) / 2};
    return QString(data, length);
}

QVariant QsStatement::getValue(const int index) const
{
    CHECK_READ_ARGS(this, "getValue", index)

    QVariant result;
    switch (sqlite3_column_type(_statement, index)) {
    case SQLITE_INTEGER:
        result = getInt64(index);
        break;
    case SQLITE_FLOAT:
        result = getDouble(index);
        break;
    case SQLITE3_TEXT:
    {
        if (dbIsUtf8Encoded()) {
            result = getText(index);
        } else {
            result = getText16(index);
        }
    }
        break;
    case SQLITE_BLOB:
        result = getBlob(index);
        break;
    default:
        break;
    }
    return result;
}

bool QsStatement::hasNonSelectType() const noexcept
{
    return isValid() && !sqlite3_stmt_readonly(_statement);
}

bool QsStatement::hasSelectType() const noexcept
{
    return isValid() && sqlite3_stmt_readonly(_statement);
}

bool QsStatement::isNull(const int index) const noexcept
{
    CHECK_READ_ARGS(this, "isNull", index)

    return sqlite3_column_type(_statement, index) == SQLITE_NULL;
}

bool QsStatement::isValid() const noexcept
{
    return _statement;
}

QByteArray QsStatement::lastError() const
{
    QByteArray result;

    if (isValid())
    {
        result = QByteArray(sqlite3_errmsg(_db));
    }

    return result;
}

QString QsStatement::lastError16() const
{
    QString result;

    if (isValid())
    {
        result = QString(reinterpret_cast<const QChar*>(sqlite3_errmsg16(_db)));
    }

    return result;
}

int QsStatement::lastErrorCode() const noexcept
{
    return isValid() ? sqlite3_errcode(_db) : -1;
}

qint64 QsStatement::lastInsertRowId() const noexcept
{
    return isValid() ? sqlite3_last_insert_rowid(_db) : 0;
}

int QsStatement::lengthAt(int index) const noexcept
{
    CHECK_READ_ARGS(this, "lengthAt", index)

    return sqlite3_column_bytes(_statement, index);
}

int QsStatement::length16At(int index) const noexcept
{
    CHECK_READ_ARGS(this, "length16At", index)

    return sqlite3_column_bytes16(_statement, index) / 2;
}

bool QsStatement::next() const noexcept
{
    CHECK_STATEMENT(this, "next")

    return isValid() && sqlite3_step(_statement) == SQLITE_ROW;
}

bool QsStatement::recompile(const QByteArray& query) noexcept
{
    return compile(query);
}

bool QsStatement::recompile(const QString& query) noexcept
{
    return compile(query);
}

QByteArray QsStatement::query() const
{
    QByteArray result;

    if (isValid())
    {
        result = QByteArray(sqlite3_sql(_statement));
    }

    return result;
}

QString QsStatement::query16() const
{
    QString result;

    if (isValid())
    {
        result = QString::fromUtf8(sqlite3_sql(_statement));
    }

    return result;
}

QsStatement::Type QsStatement::type() const noexcept
{
    if (isValid()) {
        return sqlite3_stmt_readonly(_statement)
                ? Type::Select : Type::NonSelect;
    } else {
        return Type::Undefined;
    }
}

QsStatement& QsStatement::operator=(QsStatement&& statement) noexcept
{
    if (this != &statement) {
        // clear this
        clear();

        // move data from statement to this
        _statement = statement._statement;
        _db = statement._db;
        _encoding = statement._encoding;

        // reset statement
        statement.reset();
    }
    return *this;
}

void QsStatement::reset() noexcept
{
    _statement = NULL;
    _db = NULL;
    _encoding = QsConnection::Undefined;
}

bool QsStatement::compile(const QByteArray& query) noexcept
{
    return _db
            && _encoding != QsConnection::Undefined
            && sqlite3_prepare_v2(_db, query.constData(), query.length(),
                                  &_statement, NULL) == SQLITE_OK
            && _statement;
}

bool QsStatement::compile(const QString& query) noexcept
{
    return _db
            && _encoding != QsConnection::Undefined
            && sqlite3_prepare16_v2(_db, query.constData(), query.length() * 2,
                                    &_statement, NULL) == SQLITE_OK
            && _statement;
}

bool QsStatement::dbIsUtf8Encoded() const noexcept
{
    return _encoding == QsConnection::UTF8;
}
