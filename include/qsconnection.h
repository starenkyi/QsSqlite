#ifndef QS_CONNECTION_H
#define QS_CONNECTION_H

#include <functional>
#include <memory>
#include <utility>

#include <QByteArray>
#include <QHash>
#include <QLocale>
#include <QString>

#include "sqlite3.h"
#include "qsstatement.h"


struct sqlite3;

class QsConnection
{

public:

    friend QsStatement::QsStatement(const QsConnection& connection,
                                    const QByteArray&   query) noexcept;

    friend QsStatement::QsStatement(const QsConnection& connection,
                                    const QString&      query) noexcept;

    /*
    friend void swap(QsConnection& first, QsConnection& second) noexcept
    {
        using std::swap;

        swap(first.mSize, second.mSize);
        swap(first.mArray, second.mArray);
    }
    */

    enum QueryResult {
        Ok = 0
    };

    enum ReadResult : int {
        ReadSuccess = 0,
        ConnectionIsClosed = -1,
        NoData = -2,
        EmptyData = -3,
        NullValue = -4
    };

    enum CacheMode {
        PrivateCache = 0,
        SharedCache
    };

    enum OpenMode {
        ReadWriteCreate = 0,
        ReadWrite,
        ReadOnly,
        InMemory
    };

    enum ThreadMode {
        Default,
        Serialized,
        MultiThread,
        SingleThread
    };

    enum Encoding {
        Undefined = 0,
        UTF8,
        UTF16,
        UTF16LE,
        UTF16BE,
    };

    // default CacheMode value for initializing object in constructor
    static const CacheMode defaultCacheMode { CacheMode::PrivateCache };

    // default OpenMode value for initializing object in constructor
    static const OpenMode  defaultOpenMode { OpenMode::ReadWriteCreate };

    // default ThreadMode value for initializing object in constructor
    static const ThreadMode defaultThreadMode { ThreadMode::Default };

    QsConnection(const QByteArray& dbName = QByteArray()) Q_DECL_NOTHROW;

    QsConnection(QsConnection&& connection) Q_DECL_NOTHROW;

    virtual ~QsConnection();

    void close() Q_DECL_NOTHROW;

    bool commit() Q_DECL_NOTHROW;

    bool createUtf16Collation(const QByteArray& name,
                              const QLocale&    locale);

#ifdef QS_UNICODE_COLL

    bool createIcuCollation(const QByteArray& name,
                            const QByteArray& localeOptions);

#endif

    bool deleteCollation(const QByteArray& name);

    Encoding encoding() const;

    bool execute(const QByteArray& query) const noexcept;

    bool execute(const QString& query) const;

    QByteArray databaseName() const Q_DECL_NOTHROW;

    bool isOpen() const noexcept;

    int lastErrorCode() const noexcept;

    QByteArray lastError() const;

    QString lastError16() const;

    qint64 lastInsertRowId() const noexcept;

    bool open(OpenMode   openMode   = defaultOpenMode,
              ThreadMode threadMode = defaultThreadMode,
              CacheMode  cacheMode  = defaultCacheMode);

    QsStatement prepare(const QByteArray& query) Q_DECL_NOTHROW;

    QsStatement prepare(const QString& query) Q_DECL_NOTHROW;

    std::pair<double, int> readDouble(const QByteArray& query) const;

    std::pair<double, int> readDouble(const QString& query) const;

    std::pair<qint64, int> readInt64(const QByteArray& query) const;

    std::pair<qint64, int> readInt64(const QString& query) const;

    std::pair<QByteArray, int> readString(const QByteArray& query) const;

    std::pair<QByteArray, int> readString(const QString& query) const;

    std::pair<QString, int> readString16(const QByteArray& query) const;

    std::pair<QString, int> readString16(const QString& query) const;

    bool rollback() Q_DECL_NOTHROW;

    void setDatabaseName(const QByteArray& dbName) Q_DECL_NOTHROW;

    bool setEncoding(const Encoding newEncoding);

    bool transaction() Q_DECL_NOTHROW;

    QsConnection& operator =(QsConnection&& connection) Q_DECL_NOTHROW;

    QsConnection(const QsConnection&) = delete;
    QsConnection& operator =(const QsConnection&) = delete;

private:

    sqlite3*         _db;
    mutable Encoding _encoding;
    QByteArray       _dbName;
    QByteArray       _openErrorMsg;

    class Collator;
    QHash<QByteArray, const Collator*> _collators;

    bool encodingWasSet() const;

    void deleteAllCollators();

    bool databaseWasCreated() const;

    int openInMemoryDb(CacheMode cacheMode);

    int openRegularDb(const int flags) noexcept;

    Encoding readEncoding() const;

    int readValue(const QByteArray&                          query,
                  const std::function<void (sqlite3_stmt*)>& readLambda) const;

    bool registerCollation(const QByteArray&         name,
                           std::unique_ptr<Collator> collator);

    void reset() noexcept;

};

#endif
