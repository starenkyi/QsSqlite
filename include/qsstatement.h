#ifndef QS_STATEMENT_H
#define QS_STATEMENT_H

#include <QByteArray>
#include <QChar>
#include <QString>
#include <QVariant>

#if defined(QS_LOW_API)
#include <QPair>
#endif

class  QsConnection;
struct sqlite3_stmt;
struct sqlite3;

class QsStatement
{

public:

#if defined(QS_LOW_API)

    template<typename T>
    struct Array
    {
        T* data;
        int length;

        QPair<T*, int> toPair() const noexcept
        {
            return qMakePair(data, length);
        }
    };

    using Binary = Array<unsigned char>;
    using ConstBinary = Array<const unsigned char>;

    using Utf8 = Array<char>;
    using ConstUtf8 = Array<const char>;

    using Utf16 = Array<QChar>;
    using ConstUtf16 = Array<const QChar>;

#endif

    enum DataType {
        Integer = 0,
        Double,
        Text,
        Blob,
        Null
    };

    enum Type {
        NonSelect = 0,
        Select,
        Undefined
    };

    QsStatement() noexcept;

    QsStatement(const QsConnection& connection,
                const QByteArray&   query) noexcept;

    QsStatement(const QsConnection& connection,
                const QString&      query) noexcept;

    QsStatement(QsStatement&& statement) noexcept;

    ~QsStatement();

#if defined(QS_LOW_API)

    bool bindBlob(int         index,
                  const void* value,
                  int         bytes,
                  bool        cacheValue = false) const noexcept;

#endif

    bool bindBlob(int               index,
                  const QByteArray& value,
                  bool              cacheValue = false) const noexcept;

    bool bindBool(int  index,
                  bool value) const noexcept;

    int bindCount() const noexcept;

    bool bindDouble(int    index,
                    double value) const noexcept;

    bool bindInt(int index,
                 int value) const noexcept;

    bool bindInt64(int    index,
                   qint64 value) const noexcept;

    bool bindNull(int index) const noexcept;

#if defined(QS_LOW_API)

    bool bindText(int         index,
                  const char* value,
                  int         length     = -1,
                  bool        cacheValue = false) const noexcept;

#endif

    bool bindText(int               index,
                  const QByteArray& value,
                  bool              cacheValue = false) const noexcept;

#if defined(QS_LOW_API)

    bool bindText16(int         index,
                    const void* value,
                    int         bytes      = -1,
                    bool        cacheValue = false) const noexcept;

#endif

    bool bindText16(int            index,
                    const QString& value,
                    bool           cacheValue = false) const noexcept;

    bool bindValue(int             index,
                   const QVariant& value,
                   bool            cacheValue = false) const noexcept;

    void clear() noexcept;

    void clearBindings() const noexcept;

    int columnCount() const noexcept;

    DataType columnType(int index) const noexcept;

    int encoding() const noexcept;

    bool execute() const noexcept;

    QByteArray expandedQuery() const;

    QString expandedQuery16() const;

#if defined(QS_LOW_API)

    ConstBinary getBinary(int index) const noexcept;

    Binary getBinaryCopy(int index) const;

#endif

    QByteArray getBlob(int index) const;

    bool getBool(int index) const noexcept;

    QByteArray getConvertedText(int index) const;

    QString getConvertedText16(int index) const;

#if defined(QS_LOW_API)

    ConstUtf8 getCText(int index) const noexcept;

    ConstUtf16 getCText16(int index) const noexcept;

    Utf8 getCTextCopy(int index) const;

    Utf16 getCText16Copy(int index) const;

#endif

    double getDouble(int index) const noexcept;

    int getInt(int index) const noexcept;

    qint64 getInt64(int index) const noexcept;

    QByteArray getText(int index) const;

    QString getText16(int index) const;

    QVariant getValue(int index) const;

    bool hasNonSelectType() const noexcept;

    bool hasSelectType() const noexcept;

    bool isNull(int index) const noexcept;

    bool isValid() const noexcept;

    QByteArray lastError() const;

    QString lastError16() const;

    int lastErrorCode() const noexcept;

    qint64 lastInsertRowId() const noexcept;

    int lengthAt(int index) const noexcept;

    int length16At(int index) const noexcept;

    bool next() const noexcept;

    bool recompile(const QByteArray& query) noexcept;

    bool recompile(const QString& query) noexcept;

    QByteArray query() const;

    QString query16() const;

    Type type() const noexcept;

    QsStatement& operator =(QsStatement&& statement) noexcept;

    QsStatement(const QsStatement& statement) = delete;
    QsStatement& operator =(const QsStatement& statement) = delete;

private:

    sqlite3_stmt* _statement;
    sqlite3*      _db;
    int           _encoding;

    void reset() noexcept;

    bool compile(const QByteArray& query) noexcept;

    bool compile(const QString& query) noexcept;

    bool dbIsUtf8Encoded() const noexcept;

};

#endif
